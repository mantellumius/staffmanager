import { Position } from "../../../shared/enums/Position";
import { Role } from "../../../shared/enums/Role";

export const positionToRole: Record<Position, Role> = {
	"HR-менеджер": "hr",
	"Сотрудник": "employee"
};