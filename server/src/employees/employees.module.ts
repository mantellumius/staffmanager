import { Module, forwardRef } from "@nestjs/common";
import { EmployeesController } from "./employee.controller";
import { EmployeesService } from "./employees.service";
import { SequelizeModule } from "@nestjs/sequelize";
import { Employee } from "./employees.model";
import { AuthModule } from "src/auth/auth.module";
import { AccountsModule } from "src/accounts/accounts.module";

@Module({
	controllers: [EmployeesController],
	providers: [EmployeesService],
	imports: [
		SequelizeModule.forFeature([Employee]),
		AccountsModule,
		forwardRef(() => AuthModule),
	],
	exports: [
		EmployeesService,
	]
})
export class EmployeesModule {}
