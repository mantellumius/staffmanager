import { Injectable, BadRequestException, HttpException, HttpStatus } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Employee } from "./employees.model";
import { CreateEmployeeDto } from "./dto/CreateEmployeeDto";
import { UpdateEmployeeDto } from "./dto/UpdateEmployeeDto";
import { AccountsService } from "src/accounts/accounts.service";
import { positionToRole } from "src/types/positionToRole";

@Injectable()
export class EmployeesService {
	constructor(@InjectModel(Employee) private employeesRepository: typeof Employee,
		private accountsService: AccountsService) { }

	async createEmployee(dto: CreateEmployeeDto) {
		const employee = await this.employeesRepository.create(dto);
		return employee;
	}

	async getEmployees(): Promise<Employee[]> {
		return await this.employeesRepository.findAll();
	}

	async getEmployeeById(id: number): Promise<Employee> {
		if (isNaN(id)) throw new BadRequestException("Некорректный id");
		return await this.employeesRepository.findByPk(id);
	}

	async updateEmployee(dto: UpdateEmployeeDto): Promise<UpdateEmployeeDto> {
		const result = await this.employeesRepository.update(dto, {
			where: {
				id: dto.id
			}
		});
		if (result[0] === 0) {
			throw new Error("Account not found");
		};
		this.accountsService.updateAccount({employeeId: dto.id, role: positionToRole[dto.position] });
		return dto;
	}

	async deleteEmployee(id: number): Promise<boolean> {
		const result = await this.employeesRepository.destroy({
			where: {
				id
			}
		});
		if (result === 0)
			throw new HttpException("Такого пользователя не существует", HttpStatus.NOT_FOUND);
		return true;
	}
}
