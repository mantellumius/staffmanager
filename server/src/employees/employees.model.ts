import { Column, Model, Table, DataType } from "sequelize-typescript";
import { Position } from "../../../shared/enums/Position";

interface EmployeeCreationAttrs {
	lastname: string;
	firstname: string;
	secondname: string;
	birthdate: Date;
	hiredAt: Date;
	salary: number;
	position: Position;
}

@Table({ tableName: "employees" })
export class Employee extends Model<Employee, EmployeeCreationAttrs> {
	@Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
		id: number;

	@Column({ type: DataType.STRING, allowNull: false })
		lastname: string;

	@Column({ type: DataType.STRING, allowNull: false })
		firstname: string;

	@Column({ type: DataType.STRING, allowNull: false })
		secondname: string;

	@Column({ type: DataType.DATE, allowNull: false })
		birthdate: Date;

	@Column({ type: DataType.DATE, allowNull: false, defaultValue: DataType.NOW })
		hiredAt: Date;

	@Column({ type: DataType.INTEGER, allowNull: false })
		salary: number;

	@Column({ type: DataType.ENUM<Position>("HR-менеджер", "Сотрудник"), allowNull: false, defaultValue: "Сотрудник" as Position  })
		position: Position;
}