import { Controller, Post, Body, Get, Delete, Param, Put } from "@nestjs/common";
import { CreateEmployeeDto } from "./dto/CreateEmployeeDto";
import { EmployeesService } from "./employees.service";
import { UseGuards } from "@nestjs/common";
import { Roles } from "src/auth/role-auth.decorator";
import { RolesGuard as RolesGuard } from "src/auth/roles.guard";
import { UpdateEmployeeDto } from "./dto/UpdateEmployeeDto";


@Controller("api/employees")
export class EmployeesController {

	constructor(private readonly employeesService: EmployeesService) { }

	@UseGuards(RolesGuard)
	@Roles("employee", "hr")
	@Get("/:id")
	getById(@Param("id") id: number) {
		return this.employeesService.getEmployeeById(id);
	}

	@UseGuards(RolesGuard)
	@Roles("employee", "hr")
	@Get()
	getAll() {
		return this.employeesService.getEmployees();
	}

	@UseGuards(RolesGuard)
	@Roles("hr")
	@Post()
	create(@Body() dto: CreateEmployeeDto) {
		return this.employeesService.createEmployee(dto);
	}

	@UseGuards(RolesGuard)
	@Roles("hr")
	@Put("/:id")
	update(@Param("id") id: number, @Body() dto: UpdateEmployeeDto) {
		dto.id = id;
		return this.employeesService.updateEmployee(dto);
	}

	@UseGuards(RolesGuard)
	@Roles("hr")
	@Delete("/:id")
	delete(@Param("id") id: number) {
		return this.employeesService.deleteEmployee(id);
	}
}
