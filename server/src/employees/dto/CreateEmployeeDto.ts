import { IsEnum, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Position } from "../../../../shared/enums/Position";

export class CreateEmployeeDto {
	@IsString()
	@IsNotEmpty()
		lastname: string;

	@IsString()
	@IsNotEmpty()
		firstname: string;

	@IsString()
	@IsNotEmpty()
		secondname: string;

	@IsString()
		birthdate?: Date;

	@IsString()
		hiredAt?: Date;

	@IsNumber()
	@IsNotEmpty()
		salary: number;

	@IsString()
	@IsEnum({ "HR-менеджер": "HR-менеджер", "Сотрудник": "Сотрудник" } as Record<Position, Position>)
	@IsNotEmpty()
		position: Position;
}