import { IsString,  IsNumber, IsEnum } from "class-validator";
import { Position } from "../../../../shared/enums/Position";

export class UpdateEmployeeDto {
	id: number;
	@IsString()
		lastname?: string;

	@IsString()
		firstname?: string;

	@IsString()
		secondname?: string;

	@IsString()
		birthdate?: Date;

	@IsString()
		hiredAt?: Date;

	@IsNumber()
		salary?: number;

	@IsString()
	@IsEnum({ "HR-менеджер": "HR-менеджер", "Сотрудник": "Сотрудник" } as Record<Position, Position>)
		position?: Position;
}