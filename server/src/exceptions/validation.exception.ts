import { HttpException, HttpStatus } from "@nestjs/common";

export class ValidationException extends HttpException {
	messages: string[];

	constructor(response) {
		super({ messages: response, statusCode: HttpStatus.BAD_REQUEST, error: "Bad Request" }, HttpStatus.BAD_REQUEST);		
		this.messages = response;
	}
}