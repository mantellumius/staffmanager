import { Controller, Post, Body, Patch, HttpCode, Param, UseGuards } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { CreateAccountDto } from "src/accounts/dto/CreateAccountDto";
import { CredentialsDto } from "./dto/CredentialsDto";
import { ResetPasswordDto } from "./dto/ResetPasswordDto";
import { RolesGuard } from "./roles.guard";
import { Roles } from "./role-auth.decorator";

@Controller("auth")
export class AuthController {
	constructor(private readonly authService: AuthService) { }

    @Post("/login")
    @HttpCode(200)
	login(@Body() credentials: CredentialsDto) {
		return this.authService.login(credentials);
	}

    @UseGuards(RolesGuard)
    @Roles("hr")
    @Post("/registration")
    registration(@Body() accountDto: CreateAccountDto) {
    	return this.authService.registration(accountDto);
    }

    @UseGuards(RolesGuard)
    @Roles("hr")
    @Post("/partial-registration")
    partialRegistration(@Body() accountDto: Pick<CreateAccountDto, "login" | "employeeId">) {
    	return this.authService.partialRegistration(accountDto);
    }

    @Patch("/reset-password/:token")
    resetPassword(@Body() dto: ResetPasswordDto, @Param("token") token: string) {
    	return this.authService.resetPassword(dto.newPassword, token);
    }
}
