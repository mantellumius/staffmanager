import { Injectable, CanActivate, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class JwtAuthGuard implements CanActivate {
	constructor(private readonly jwtService: JwtService) { }

	canActivate(context: any): boolean | Promise<boolean> {
		const req = context.switchToHttp().getRequest();
		try {
			const authHeader = req.headers.authorization;
			const [bearer, token] = authHeader.split(" ");
			if (bearer !== "Bearer" || !token) {
				throw new UnauthorizedException({message: "Пользователь не авторизован"});
			}
			return true;
		} catch (e) {
			throw new UnauthorizedException({ message: "Пользователь не авторизован" });
		}
	}
}