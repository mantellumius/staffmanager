import { SetMetadata } from "@nestjs/common";
import { Role } from "../../../shared/enums/Role";

export const ROLE_KEY = "role";

export const Roles = (...roles: Role[]) => SetMetadata(ROLE_KEY, roles);