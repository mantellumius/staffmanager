import { IsString, Length } from "class-validator";

export class CredentialsDto {
	@IsString()
	@Length(1, undefined, { message: "Длина логина должна быть больше 1 символов" })
		login: string;

	@IsString()
	@Length(1, undefined, { message: "Длина пароля должна быть больше 1 символов" })
		password: string;
}