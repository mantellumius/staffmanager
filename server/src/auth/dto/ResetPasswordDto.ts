import { IsString, Length } from "class-validator";

export class ResetPasswordDto {
	@IsString({ message: "Должен быть строкой" })
	@Length(8, 20, { message: "Должен быть от 8 до 20 символов" })
		newPassword: string;
}