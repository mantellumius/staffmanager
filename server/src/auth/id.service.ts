import { Injectable } from "@nestjs/common";

@Injectable()
export class IdService {
	constructor() { }

	private alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public generateId(): string {
		return `${this.S4()}-${this.S4()}-${this.S4()}-${this.S4()}`;
	}

	private S4() {
		return `${this.generateSymbol()}${this.generateSymbol()}${this.generateSymbol()}${this.generateSymbol()}`;
	}

	private generateSymbol(): string {
		return this.alphabet[Math.floor(Math.random() * this.alphabet.length)];
	}
}
