import { Injectable, HttpException, HttpStatus, UnauthorizedException, Inject } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { AccountsService } from "src/accounts/accounts.service";
import { CreateAccountDto } from "src/accounts/dto/CreateAccountDto";
import * as bcrypt from "bcryptjs";
import { Account } from "src/accounts/account.model";
import { EmployeesService } from "src/employees/employees.service";
import { CredentialsDto } from "./dto/CredentialsDto";
import { Cache } from "cache-manager";
import { CACHE_MANAGER } from "@nestjs/cache-manager";
import { IdService } from "./id.service";

@Injectable()
export class AuthService {
	constructor(private readonly accountService: AccountsService,
		private readonly employeesService: EmployeesService,
		private readonly jwtService: JwtService,
		private readonly idService: IdService,
		@Inject(CACHE_MANAGER) private readonly cacheManager: Cache) { }

	async login(credentials: CredentialsDto) {
		const account = await this.validateCredentials(credentials);
		return this.generateToken(account);
	}

	async registration(accountDto: CreateAccountDto) {
		const candidate = (await this.accountService.getAccountByLogin(accountDto.login)) ||
			(await this.accountService.getAccountByEmployeeId(accountDto.employeeId));
		if (candidate) throw new HttpException("Аккаунт уже существует", HttpStatus.BAD_REQUEST);
		const employee = await this.employeesService.getEmployeeById(accountDto.employeeId);
		if (!employee) throw new HttpException("Сотрудник не найден", HttpStatus.NOT_FOUND);
		const hashedPassword = await bcrypt.hash(accountDto.password, 5);
		const account = await this.accountService.createAccount({ ...accountDto, password: hashedPassword });
		return this.generateToken(account);
	}

	async partialRegistration(accountDto: Pick<CreateAccountDto, "login" | "employeeId">) {
		const candidate = (await this.accountService.getAccountByLogin(accountDto.login)) ||
			(await this.accountService.getAccountByEmployeeId(accountDto.employeeId));
		if (candidate) throw new HttpException("Аккаунт с таким логином уже существует", HttpStatus.BAD_REQUEST);
		const employee = await this.employeesService.getEmployeeById(accountDto.employeeId);
		if (!employee) throw new HttpException("Сотрудник не найден", HttpStatus.NOT_FOUND);
		const account = await this.accountService.createAccount({ ...accountDto, role: employee.position === "HR-менеджер" ? "hr" : "employee", password: "" });
		return this.generatePasswordResetToken(account);
	}

	generateToken(account: Account) {
		const payload = {
			login: account.login,
			id: account.employeeId,
			role: account.role
		};
		return {
			accessToken: this.jwtService.sign(payload)
		};
	}

	async generatePasswordResetToken(account: Account) {
		const key = this.idService.generateId();
		this.cacheManager.set(`passwordResetTokens:${key}`, account, { ttl: 60 * 60 * 24 } as unknown as number);
		return key;
	}

	private async validateCredentials(credentials: CredentialsDto) {
		const account = await this.accountService.getAccountByLogin(credentials.login);
		if (!account) throw new UnauthorizedException({ message: "Пользователя с таким логином не существует" });
		const passwordEquals = await bcrypt.compare(credentials.password, account.password);
		if (account && passwordEquals) {
			return account;
		}
		throw new UnauthorizedException({ message: "Неверный логин или пароль" });
	}

	async resetPassword(newPassword: string, resetToken: string) {
		const account = (await this.cacheManager.get(`passwordResetTokens:${resetToken}`)) as Account;
		if (!account) throw new HttpException("Неверный токен", HttpStatus.BAD_REQUEST);
		const hashedPassword = await bcrypt.hash(newPassword, 5);
		const updatedAccount = await this.accountService.updateAccount({ login: account.login, password: hashedPassword });
		if (updatedAccount)
			await this.cacheManager.del(`passwordResetTokens:${resetToken}`);
	}
}
