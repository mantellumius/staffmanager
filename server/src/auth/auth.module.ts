import { Module, forwardRef } from "@nestjs/common";
import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { JwtModule } from "@nestjs/jwt";
import { AccountsModule } from "src/accounts/accounts.module";
import { JwtAuthGuard } from "./jwt-auth.guard";
import { EmployeesModule } from "src/employees/employees.module";
import { IdService } from "./id.service";

@Module({
	providers: [AuthService, JwtAuthGuard, IdService],
	controllers: [AuthController],
	imports: [
		AccountsModule,
		forwardRef(() => EmployeesModule),
		JwtModule.register({
			secret: process.env.JWT_SECRET || "SECRET",
			signOptions: {
				expiresIn: "24h",
			}
		}),
	],
	exports: [
		JwtAuthGuard,
		AuthService,
		JwtModule
	]
})
export class AuthModule { }
