import { Injectable, CanActivate, UnauthorizedException, HttpException, HttpStatus } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { JwtService } from "@nestjs/jwt";
import { ROLE_KEY } from "./role-auth.decorator";
import { Account } from "src/accounts/account.model";

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private readonly jwtService: JwtService,
		private readonly reflector: Reflector) { }

	canActivate(context: any): boolean | Promise<boolean> {
		const req = context.switchToHttp().getRequest();
		try {
			const authHeader = req.headers.authorization;
			const [bearer, token] = authHeader.split(" ");
			if (bearer !== "Bearer" || !token) {
				throw new UnauthorizedException({ message: "Пользователь не авторизован" });
			}
			const account = this.jwtService.verify(token) as Account;
			req.account = account;
			const requiredRoles = this.reflector.getAllAndOverride<string[]>(ROLE_KEY, [
				context.getHandler(),
				context.getClass(),
			]);
			if (!requiredRoles) return true;
			return requiredRoles.includes(account.role);
		} catch (e) {
			console.log(e);
			throw new HttpException("Нет доступа", HttpStatus.FORBIDDEN);
		}
	}
}