import { CreateAccountDto } from "./dto/CreateAccountDto";
import { AccountsService } from "./accounts.service";
import { Controller, Post, Body, Get, Put, Param, BadRequestException, NotFoundException } from "@nestjs/common";
import { UpdateAccountDto } from "./dto/UpdateAccountDto";
import { RolesGuard } from "src/auth/roles.guard";
import { Roles } from "src/auth/role-auth.decorator";
import { UseGuards } from "@nestjs/common/decorators";

@UseGuards(RolesGuard)
@Roles("hr")
@Controller("accounts")
export class AccountsController {

	constructor(private readonly accountsService: AccountsService) { }

	@Post()
	create(@Body() dto: CreateAccountDto) {
		return this.accountsService.createAccount(dto);
	}

	@Get()
	getAll() {
		return this.accountsService.getAccounts();
	}

	@Get("/:id")
	async getAccountByEmployeeId(@Param("id") id: number) {
		if (isNaN(id)) throw new BadRequestException("Неверный id");
		const account = await this.accountsService.getAccountByEmployeeId(id);
		if (!account) {
			throw new NotFoundException(`Пользователь с id = ${id} не найден`);
		}
		return account;
	}

	@Put()
	updateAccount(@Body() dto: UpdateAccountDto): Promise<UpdateAccountDto> {
		return this.accountsService.updateAccount(dto);
	}
}
