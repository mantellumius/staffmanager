import { IsEnum, IsNumber, IsString, Length } from "class-validator";
import { Role } from "../../../../shared/enums/Role";

export class CreateAccountDto {
	@IsNumber({allowInfinity: false, allowNaN: false},{message: "Должен быть числом"})
		employeeId: number;

	@IsString({message: "Должен быть строкой"})
		login: string;

	@IsString({ message: "Должен быть строкой" })
	@Length(8, 20, { message: "Должен быть от 8 до 20 символов" })
		password: string;

	@IsString({ message: "Должен быть строкой" })
	@IsEnum({ "employee": "employee", "hr": "hr" } as Record<Role, Role>)
		role?: Role;
}