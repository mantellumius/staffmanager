import { IsEnum, IsNotEmpty, IsNumber, IsString, ValidateIf } from "class-validator";
import { Role } from "../../../../shared/enums/Role";

export class UpdateAccountDto {
	@ValidateIf(o => !o.login)
	@IsNumber()
	@IsNotEmpty()
		employeeId?: number;

	@ValidateIf(o => !o.employeeId)
	@IsString()
	@IsNotEmpty()
		login?: string;

	@IsString()
		password?: string;

	@IsString({ message: "Должен быть строкой" })
	@IsEnum({ "employee": "employee", "hr": "hr" } as Record<Role, Role>)
		role?: Role;
}