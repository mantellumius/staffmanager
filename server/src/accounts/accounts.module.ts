import { Module } from "@nestjs/common";
import { AccountsService } from "./accounts.service";
import { AccountsController } from "./accounts.controller";
import { SequelizeModule } from "@nestjs/sequelize";
import { forwardRef } from "@nestjs/common/utils";
import { Account } from "./account.model";
import { Employee } from "src/employees/employees.model";
import { AuthModule } from "src/auth/auth.module";

@Module({
	providers: [AccountsService],
	controllers: [AccountsController],
	imports: [
		SequelizeModule.forFeature([Account, Employee]),
		forwardRef(() => AuthModule),
	],
	exports: [
		AccountsService,
	]
})
export class AccountsModule {}
