import { Column, Model, Table, DataType, ForeignKey, BelongsTo } from "sequelize-typescript";
import { Employee } from "src/employees/employees.model";
import { Role } from "../../../shared/enums/Role";

interface AccountCreationAttrs {
    id: number;
    login: string;
    role: Role;
    password?: string;
}


@Table({ tableName: "accounts" })
export class Account extends Model<Account, AccountCreationAttrs> {
    @ForeignKey(() => Employee)
    @Column({ type: DataType.INTEGER, primaryKey: true, unique: true })
    	employeeId: number;

    @BelongsTo(() => Employee, { onDelete: "CASCADE" })
    	employee: Employee;

    @Column({ type: DataType.STRING, allowNull: false })
    	login: string;

    @Column({ type: DataType.STRING, allowNull: false, defaultValue: "" })
    	password: string;

    @Column({ type: DataType.ENUM<Role>("employee", "hr"), allowNull: false, defaultValue: "employee" })
    	role: Role;
}