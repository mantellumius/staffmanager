import { InjectModel } from "@nestjs/sequelize";
import { Injectable } from "@nestjs/common";
import { Account } from "./account.model";
import { CreateAccountDto } from "./dto/CreateAccountDto";
import { UpdateAccountDto } from "./dto/UpdateAccountDto";

@Injectable()
export class AccountsService {
	constructor(@InjectModel(Account) private accountRepository: typeof Account) { }

	async createAccount(dto: CreateAccountDto) {
		const account = await this.accountRepository.create(dto);
		return account;
	}

	async getAccounts(): Promise<Account[]> {
		return await this.accountRepository.findAll();
	}

	async getAccountByEmployeeId(employeeId: number): Promise<Account> {
		return await this.accountRepository.findOne({
			where: {
				employeeId: employeeId
			}
		});
	}

	async getAccountByLogin(login: string): Promise<Account> {
		return await this.accountRepository.findOne({
			where: {
				login
			}
		});
	}

	async updateAccount(dto: UpdateAccountDto): Promise<UpdateAccountDto> {
		if (!dto.login && !dto.employeeId) throw new Error("Login or employeeId must be specified");
		const account = await this.accountRepository.findOne({
			where: dto.login ? {
				login: dto.login
			} : {
				employeeId: dto.employeeId,
			}
		});
		if (!account) return;
		if (dto.role) account.role = dto.role;
		if (dto.password) account.password = dto.password;
		if (dto.login) account.login = dto.login;
		account.save();
		return dto;
	}
}
