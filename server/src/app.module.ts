import { Module } from "@nestjs/common";
import { CacheModule, CacheStore } from "@nestjs/cache-manager";
import { SequelizeModule } from "@nestjs/sequelize";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { EmployeesModule } from "./employees/employees.module";
import { AccountsModule } from "./accounts/accounts.module";
import { Account } from "./accounts/account.model";
import { Employee } from "./employees/employees.model";
import { AuthModule } from "./auth/auth.module";
import { redisStore } from "cache-manager-redis-store";
import { FiredModule } from "./fired/fired.module";
import { StatsModule } from "./stats/stats.module";
import { SeederModule } from "./seeder/seeder.module";

@Module({
	imports: [
		ConfigModule.forRoot({
			envFilePath: `.${process.env.NODE_ENV}.env`,
		}),
		SequelizeModule.forRoot({
			dialect: "postgres",
			host: process.env.POSTGRES_HOST,
			port: Number(process.env.POSTGRES_PORT),
			username: process.env.POSTGRES_USER,
			password: process.env.POSTGRES_PASSWORD,
			database: process.env.POSTGRES_DB,
			models: [Employee, Account],
			autoLoadModels: true,
		}),
		CacheModule.registerAsync({
			isGlobal: true,
			imports: [ConfigModule],
			useFactory: async (configService: ConfigService) => ({
				store: (await redisStore({
					socket: {
						host: configService.get("REDIS_HOST"),
						port: configService.get("REDIS_PORT"),
					},
					password: configService.get("REDIS_PASSWORD"),
					ttl: 60,
				})) as unknown as CacheStore,
			}),
			inject: [ConfigService],
		}),
		EmployeesModule,
		AccountsModule,
		AuthModule,
		FiredModule,
		StatsModule,
		SeederModule,
	],
	controllers: [],
	providers: [],
})
export class AppModule { }
