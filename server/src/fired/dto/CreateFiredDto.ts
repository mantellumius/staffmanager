import { IsString } from "class-validator";

export class CreateFiredDto {
	@IsString()
		reason: string;
}