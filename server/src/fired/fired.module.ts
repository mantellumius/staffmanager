import { Module } from "@nestjs/common";
import { FiredService } from "./fired.service";
import { FiredController } from "./fired.controller";
import { EmployeesModule } from "src/employees/employees.module";
import { Fired } from "./fired.model";
import { SequelizeModule } from "@nestjs/sequelize";

@Module({
	providers: [FiredService],
	controllers: [FiredController],
	imports: [
		SequelizeModule.forFeature([Fired]),
		EmployeesModule,
	]
})
export class FiredModule {}
