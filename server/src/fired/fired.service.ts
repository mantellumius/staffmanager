import { Injectable } from "@nestjs/common";
import { Fired } from "./fired.model";
import { InjectModel } from "@nestjs/sequelize";
import { EmployeesService } from "src/employees/employees.service";

@Injectable()
export class FiredService {
	constructor(@InjectModel(Fired) private firedRepository: typeof Fired,
		private employeesService: EmployeesService) { }

	async fireAnEmployee(id: number, reason: string) {
		const employee = await this.employeesService.getEmployeeById(id);
		if (!employee) return;
		const deleted = await this.employeesService.deleteEmployee(id);
		if (!deleted) return;
		const fired = await this.firedRepository.create({
			firstname: employee.firstname, 
			lastname: employee.lastname, 
			secondname: employee.secondname,
			hiredAt: employee.hiredAt,
			reason
		});
		return fired;
	}
}
