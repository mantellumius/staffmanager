import { Column, Model, Table, DataType } from "sequelize-typescript";

interface FiredCreationAttrs {
	lastname: string;
	firstname: string;
	secondname: string;
	hiredAt: Date;
	reason: string;
}

@Table({ tableName: "fired", updatedAt: false })
export class Fired extends Model<Fired, FiredCreationAttrs> {
	@Column({ type: DataType.INTEGER, unique: true, autoIncrement: true, primaryKey: true })
		id: number;

	@Column({ type: DataType.STRING, allowNull: false })
		lastname: string;

	@Column({ type: DataType.STRING, allowNull: false })
		firstname: string;

	@Column({ type: DataType.STRING, allowNull: false })
		secondname: string;

	@Column({ type: DataType.TEXT, allowNull: true })
		reason: string;

	@Column({ type: DataType.DATE, allowNull: false })
		hiredAt: Date;

	@Column({ type: DataType.DATE, allowNull: false, defaultValue: DataType.NOW })
		firedAt: Date;
}
