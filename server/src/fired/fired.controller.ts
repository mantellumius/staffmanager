import { Controller, Post, Param, Body } from "@nestjs/common";
import { FiredService } from "./fired.service";
import { CreateFiredDto } from "./dto/CreateFiredDto";

@Controller("/api/fired")
export class FiredController {
	
	constructor(private readonly firedService: FiredService) {}
	
	@Post("/:id")
	async fireAnEmployee(@Param("id") id: number, @Body()dto: CreateFiredDto) {
		await this.firedService.fireAnEmployee(id, dto.reason);
	}
}
