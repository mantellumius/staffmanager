import { Controller, Get, UseGuards } from "@nestjs/common";
import { StatsService } from "./stats.service";
import { RolesGuard } from "src/auth/roles.guard";
import { Roles } from "src/auth/role-auth.decorator";

@UseGuards(RolesGuard)
@Roles("hr")
@Controller("/api/stats")
export class StatsController { 
	
	constructor(private readonly statsService: StatsService) {}

	@Get("/hired")
	async getHiredStats() {
		const hiredStats = await this.statsService.getHiredStats();
		return hiredStats[0];
	}

	@Get("/fired")
	async getFiredStats() {
		const firedStats = await this.statsService.getFiredStats();
		return firedStats[0];
	}

	@Get("/birthdates")
	async getNearBitrhdates() {
		return await this.statsService.getNearBirthdates();
	}

	@Get("/salary")
	async getSalaryStats() {
		return (await this.statsService.getSalaryStats())[0];
	}
}
