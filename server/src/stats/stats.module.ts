import { Module } from "@nestjs/common";
import { StatsController } from "./stats.controller";
import { StatsService } from "./stats.service";
import { AuthModule } from "src/auth/auth.module";

@Module({
	controllers: [StatsController],
	providers: [StatsService],
	imports: [
		AuthModule
	]
})
export class StatsModule { }
