import { Injectable } from "@nestjs/common";
import { Sequelize } from "sequelize-typescript";

@Injectable()
export class StatsService {
	queries = {};

	constructor(private readonly sequelize: Sequelize) {
		this.queries = {
			"get-hired-stats": `WITH "hired_stats" AS (
	SELECT date_trunc('month', "hiredAt") as "hired_at_month",
		date_trunc('year', "hiredAt") as "hired_at_year"
	FROM employees
	UNION ALL
	SELECT date_trunc('month', "hiredAt") as "hired_at_month",
		date_trunc('year', "hiredAt") as "hired_at_year"
	FROM fired
)
SELECT count(*) FILTER (
		WHERE hired_stats.hired_at_year = date_trunc('year', now())
	) as "hired_this_year",
	count(*) FILTER (
		WHERE hired_stats.hired_at_month = date_trunc('month', now())
	) as "hired_this_month"
FROM hired_stats`,
			"get-fired-stats": `WITH fired_stats AS (
	SELECT date_trunc('month', fired."firedAt") as "fired_at_month",
		date_trunc('year', fired."firedAt") as "fired_at_year"
	FROM fired
)
SELECT count(*) FILTER (
		WHERE fired_stats.fired_at_year = date_trunc('year', now())
	) as "fired_this_year",
	count(*) FILTER (
		WHERE fired_stats.fired_at_month = date_trunc('month', now())
	) as "fired_this_month"
FROM fired_stats
			`,
			"get-near-bitrhdays": `WITH birthdates as (
	SELECT lastname,
	firstname,
	secondname,
		birthdate,
		(
			EXTRACT(
				DOY
				FROM birthdate
			) - EXTRACT(
				DOY
				FROM CURRENT_DATE
			)
		) as diff
	FROM employees
)
SELECT *,
	diff as "daysLeft"
FROM birthdates
WHERE diff >= 0
	and diff < 30
ORDER BY diff ASC
			`,
			"get-salary-stats": "SELECT sum(salary) as totalSalary FROM employees"
		};
	}

	async getHiredStats() {
		return (await this.sequelize.query(this.queries["get-hired-stats"]))[0];
	}

	async getFiredStats() {
		return (await this.sequelize.query(this.queries["get-fired-stats"]))[0];
	}

	async getNearBirthdates() {
		return (await this.sequelize.query(this.queries["get-near-bitrhdays"]))[0];
	}

	async getSalaryStats() {
		return (await this.sequelize.query(this.queries["get-salary-stats"]))[0];
	}
}
