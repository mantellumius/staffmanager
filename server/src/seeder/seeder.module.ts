import { Module } from "@nestjs/common";
import { SeederService } from "./seeder.service";
import { AccountsModule } from "src/accounts/accounts.module";
import { EmployeesModule } from "src/employees/employees.module";

@Module({
	providers: [SeederService],
	controllers: [],
	imports: [
		AccountsModule,
		EmployeesModule,
	]
})
export class SeederModule {}
