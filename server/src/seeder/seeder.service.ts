import { Injectable } from "@nestjs/common";
import { AccountsService } from "src/accounts/accounts.service";
import { EmployeesService } from "src/employees/employees.service";

@Injectable()
export class SeederService {
	constructor(private accountsService: AccountsService,
		private employeesService: EmployeesService) { 
		this.seed();
	}
		

	async seed() {
		const amount = await this.employeesService.getEmployees();
		if (amount[0]) return;
		const employee = await this.employeesService.createEmployee({
			lastname: "Иванов",
			firstname: "Иван",
			secondname: "Иванович",
			salary: 1234,
			birthdate: new Date(2000, 2, 20),
			hiredAt: new Date(2023, 7, 18),
			position: "HR-менеджер"
		});
		this.accountsService.createAccount(
			{
				employeeId: employee.id,
				login: "Иванов Иван Иванович",
				password: "$2a$05$CkC08yXPVzsAdD5VWhZxV.0PctfoXnuwMkdMq5qtcjMLvMyahWR/G", // password123
				role: "hr"
			});
	}
}
