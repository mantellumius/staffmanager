# Менеджер сотрудников
Проект выполнен как тестовое задание, для компании CaseGuru.

## Стек
#### Frontend
- React(Vite)
- mobx
- MUI
#### Backend
- Nest.js
- Sequelize
- PostgreSQL
- Redis
#### Other
- Docker
- nginx

## Установка
```shell
git clone https://gitlab.com/mantellumius/staffmanager.git
```
```shell
docker-compose up
```
http://localhost

Логин: Иванов Иван Иванович

Пароль: password123

## Вопросы и принятые решения
- Почему выплаты зп это график, если это всегда прямая? Принял решение сделать ожидаемые выплаты за ближайший месяц вместо графика.
- Сделал авторизацию через роли, вместо должностей. При изменении должности меняется роль.