export default function useLoadingWrapper<T>(func: () => T, setLoading: (loading: boolean) => void) {
	return async () => {
		setLoading(true);
		const result = await func();
		setLoading(false);
		return result;
	};
}