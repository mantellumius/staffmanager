export type User = {
	login: string,
	role: string,
	id: number,
}