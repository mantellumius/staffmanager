import { Position } from "../../../shared/enums/Position";

export type Employee = {
	id?: number;
	lastname: string;
	firstname: string;
	secondname: string;
	birthdate: moment.Moment;
	hiredAt: moment.Moment;
	salary: number;
	position: Position;
	createdAt?: moment.Moment,
	updatedAt?: moment.Moment,
}