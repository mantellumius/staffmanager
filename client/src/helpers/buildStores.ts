import AuthStore from "../stores/AuthStore";
import EmployeesStore from "../stores/EmployeesStore";
import NavBarStore from "../stores/NavBarStore";
import StatsStore from "../stores/StatsStore";

export default function buildStores() {
	const stores = {} as Stores;
	stores.auth = new AuthStore();
	stores.employees = new EmployeesStore();
	stores.navBar = new NavBarStore();
	stores.stats = new StatsStore();
	return stores;
}

export type Stores = {
	employees: EmployeesStore,
	auth: AuthStore,
	navBar: NavBarStore,
	stats: StatsStore
}