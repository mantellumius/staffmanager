import moment from "moment";

export default function formatMoment(moment: moment.Moment) {
	return moment.format("DD.MM.YYYY");
}