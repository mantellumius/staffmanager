import { Routes, Route } from "react-router-dom";
import Login from "../pages/Auth/Login";
import ResetPassword from "../pages/Auth/ResetPassword";
import Staff from "../pages/Staff/Staff";
import { Stores } from "../helpers/buildStores";
import StaffAdd from "../pages/Staff/StaffAdd";
import StaffEdit from "../pages/Staff/StaffEdit";
import Stats from "../pages/Stats/Stats";
import Profile from "../pages/Profile/Profile";
import StaffAddedSuccess from "../pages/Staff/StaffAddedSuccess";

export default function MainRouter({ stores }: { stores: Stores }) {
	return (
		<Routes>
			<Route path="/login" element={<Login authStore={stores.auth}/>} />
			<Route path="/reset-password/:token" element={<ResetPassword authStore={stores.auth}/>} />
			<Route path="/staff" element={<Staff employeesStore={stores.employees} authStore={stores.auth}/>} />
			<Route path="/staff/add" element={<StaffAdd employeesStore={stores.employees}/>} />
			<Route path="/staff/add/success" element={<StaffAddedSuccess />} />
			<Route path="/staff/edit" element={<StaffEdit employeesStore={stores.employees} />} />
			<Route path="/stats" element={<Stats statsStore={stores.stats}/>} />
			<Route path="/profile" element={<Profile authStore={stores.auth} />} />
		</Routes>
	);
}
