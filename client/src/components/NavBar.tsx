import { styled } from "@mui/material/styles";
import Drawer from "@mui/material/Drawer";
import MuiAppBar, { AppBarProps as MuiAppBarProps } from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import { useNavigate } from "react-router-dom";
import AuthStore from "../stores/AuthStore";
import LogoutIcon from "@mui/icons-material/Logout";
import LoginIcon from "@mui/icons-material/Login";
import { observer } from "mobx-react-lite";
import NavBarStore from "../stores/NavBarStore";
import Button from "@mui/material/Button";

const drawerWidth = 240;

interface AppBarProps extends MuiAppBarProps {
	open?: boolean;
}

const AppBar = styled(MuiAppBar, {
	shouldForwardProp: (prop) => prop !== "open",
})<AppBarProps>(({ theme, open }) => ({
	transition: theme.transitions.create(["margin", "width"], {
		easing: theme.transitions.easing.sharp,
		duration: theme.transitions.duration.leavingScreen,
	}),
	...(open && {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: `${drawerWidth}px`,
	}),
}));

const NavBar = observer(({ authStore, navBarStore }: { authStore: AuthStore, navBarStore: NavBarStore }) => {
	const navigate = useNavigate();

	return (
		<>
			<AppBar position="fixed" open={navBarStore.open}>
				<Toolbar>
					<IconButton
						color="inherit"
						aria-label="open drawer"
						onClick={() => navBarStore.toggleAppBar()}
						edge="start"
					>
						<MenuIcon />
					</IconButton>
					<Typography variant="h6" sx={{ flexGrow: 1 }} noWrap component="div">
						Менеджер сотрудников
					</Typography>
					
					{authStore.authenticated ?
						<>
							<Typography fontWeight={700}>
								{authStore.user?.login}
							</Typography>
							<IconButton color="inherit" edge="end" onClick={() => authStore.logout()}>
								<LogoutIcon />
							</IconButton>
						</> :
						<>
							<Button variant="contained" onClick={() => navigate("/login")} style={{lineHeight: "normal"}}>
								Войти
								<LoginIcon/>
							</Button>
						</>
					}
				</Toolbar>
			</AppBar>
			<Drawer
				sx={{
					width: drawerWidth,
					flexShrink: 0,
					"& .MuiDrawer-paper": {
						width: drawerWidth,
						boxSizing: "border-box",
					},
				}}
				variant="persistent"
				anchor="left"
				open={navBarStore.open}
			>
				<Divider />
				<List>
					{navBarStore.items
						.filter(item => !item.reqRole || item.reqRole === authStore.user?.role)
						.map((item) => (
							<ListItem key={item.id} disablePadding onClick={() => navigate(item.route)}>
								<ListItemButton disabled={item.reqAuth && !authStore.authenticated}>
									<ListItemIcon>
										{item.icon}
									</ListItemIcon>
									<ListItemText primary={item.label} />
								</ListItemButton>
							</ListItem>
						))}
				</List>
			</Drawer>
		</>
	);
});

export default NavBar;