import { Box, Paper, Stack, Typography } from "@mui/material";

const StatCard = ({ values, title }: { values: string[], title: string }) => {
	return (
		<Paper variant={"elevation"}>
			<Box minHeight={125} minWidth={300} padding={3}>
				<Typography fontWeight={700} fontSize={16}>
					{title}
				</Typography>
				<Stack marginTop={2}>
					{values.map(value => (
						<Typography key={value} fontSize={18}>
							{value}
						</Typography>
					))}
				</Stack>
			</Box>
		</Paper>
	);
};

export default StatCard;