import * as React from "react";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import moment from "moment";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import { observer } from "mobx-react-lite";
import { Position } from "../../../shared/enums/Position";
import EmployeesStore from "../stores/EmployeesStore";
import { LoadingButton } from "@mui/lab";

const StaffForm: React.FC<Props> = observer(({ employeesStore, title, buttonText, loading, handleSubmit }) => {
	return (
		<LocalizationProvider dateAdapter={AdapterMoment}>
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}>
				<Typography component="h1" variant="h5" marginBottom={"1em"} color={"primary.main"} fontWeight={600}>
					{title}
				</Typography>
				<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1, display: "flex", flexDirection: "column", gap: "1em", width: "clamp(300px, 40vw, 500px)" }}>
					<TextField
						required
						fullWidth
						name="lastname"
						label="Фамилия"
						id="lastname"
						autoFocus
						autoComplete="lastname"
						value={employeesStore.currentEmployee.lastname}
						onChange={(e) => employeesStore.currentEmployee.lastname = e.target.value}
					/>
					<TextField
						margin="normal"
						required
						fullWidth
						id="firstname"
						label="Имя"
						name="firstname"
						autoComplete="firstname"
						value={employeesStore.currentEmployee.firstname}
						onChange={(e) => employeesStore.currentEmployee.firstname = e.target.value}
					/>
					<TextField
						required
						fullWidth
						name="secondname"
						label="Отчество"
						id="secondname"
						autoComplete="secondname"
						value={employeesStore.currentEmployee.secondname}
						onChange={(e) => employeesStore.currentEmployee.secondname = e.target.value}
					/>
					<DatePicker
						sx={{
							mt: "1em",
							mb: ".5em",
						}}
						label="Дата рождения"
						value={employeesStore.currentEmployee.birthdate ?? moment.now}
						onChange={(e) => employeesStore.currentEmployee.birthdate = e ?? moment()} />
					<Divider />
					<DatePicker
						sx={{
							mt: "1em",
						}}
						label="Нанят"
						value={employeesStore.currentEmployee.hiredAt ?? moment.now}
						onChange={(e) => employeesStore.currentEmployee.hiredAt = e ?? moment()} />
					<FormControl margin="normal">
						<InputLabel id="position-select-label">Должность</InputLabel>
						<Select
							labelId="position-select-label"
							id="position-select"
							value={employeesStore.currentEmployee.position}
							label="Должность"
							onChange={(e: SelectChangeEvent) => employeesStore.currentEmployee.position = e.target.value as Position}
						>
							<MenuItem value={"Сотрудник"}>Сотрудник</MenuItem>
							<MenuItem value={"HR-менеджер"}>HR-менеджер</MenuItem>
						</Select>
					</FormControl>
					<TextField
						margin="normal"
						required
						fullWidth
						name="salary"
						label="Зарплата"
						type="number"
						id="salary"
						autoComplete="salary"
						value={employeesStore.currentEmployee.salary}
						onChange={(e) => employeesStore.currentEmployee.salary = Math.max(0, isNaN(parseInt(e.target.value)) ? 0 : parseInt(e.target.value))}
					/>
					<LoadingButton
						loading={loading}
						type="submit"
						fullWidth
						variant="contained"
					>
						{buttonText}
					</LoadingButton>
				</Box>
			</Box>
		</LocalizationProvider>
	);
});
type Props = {
	employeesStore: EmployeesStore,
	title: string, buttonText: string, 
	loading: boolean,
	handleSubmit: (event: React.FormEvent<HTMLFormElement>) => void
}
export default StaffForm;