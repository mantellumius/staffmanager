import * as React from "react";
import { styled } from "@mui/system";
import { useTheme } from "@emotion/react";
import { Theme } from "@mui/material/styles";
import NavBarStore from "../stores/NavBarStore";
import { observer } from "mobx-react-lite";

const drawerWidth = 240;
const MainStyled = styled("main")(({ theme, open }: { theme: Theme, open: boolean }) => ({
	width: `calc(100vw - ${(open ? drawerWidth : 0) + 20}px)`,
	height: "calc(100vh - 60px)",
	marginTop: "60px",
	padding: "60px",
	display: "flex",
	alignItems: "flex-start",
	justifyContent: "center",
	transition: `all ${theme.transitions.duration.enteringScreen}ms ${theme.transitions.easing.easeOut}`,
	...(open && {
		marginLeft: drawerWidth,
	}),
}));

const Main = observer(({ children, navBarStore }: { children: React.ReactNode, navBarStore: NavBarStore }) => {
	const theme = useTheme();

	return (
		<MainStyled theme={theme as any} open={navBarStore?.open ?? false}>
			{children}
		</MainStyled>
	);
});

export default Main;
