import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import { BrowserRouter } from "react-router-dom";
import buildStores from "./helpers/buildStores.ts";
import { ThemeProvider } from "@emotion/react";
import theme from "./theme.ts";

const stores = buildStores();
ReactDOM.createRoot(document.getElementById("root")!).render(
	<React.StrictMode>
		<ThemeProvider theme={theme}>
			<BrowserRouter>
				<App stores={stores}/>
			</BrowserRouter>
		</ThemeProvider>
	</React.StrictMode>,
);
