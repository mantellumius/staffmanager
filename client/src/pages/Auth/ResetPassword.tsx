import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import AuthStore from "../../stores/AuthStore";
import { useNavigate, useParams } from "react-router-dom";


const ResetPassword = ({ authStore }: { authStore: AuthStore }) => {
	const [credentials, setCredentials] = React.useState<{ login: string, password: string }>({ login: "", password: "" });
	const { token } = useParams() as { token: string };
	const navigate = useNavigate();

	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const success = await authStore.resetPassword(token, credentials.password);
		if (success)
			navigate("/");
	};

	return (
		<Container component="main" maxWidth="xs">
			<Box
				sx={{
					marginTop: 8,
					display: "flex",
					flexDirection: "column",
					alignItems: "center",
				}}>
				<Typography component="h1" variant="h5">
					Придумайте новый пароль
				</Typography>
				<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
					<TextField
						margin="normal"
						required
						fullWidth
						name="password"
						label="Пароль"
						type="password"
						id="password"
						autoComplete="current-password"
						value={credentials.password}
						onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
					/>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
					>
						Обновить пароль
					</Button>
				</Box>
			</Box>
		</Container>
	);
};

export default ResetPassword;