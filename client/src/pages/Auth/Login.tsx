import * as React from "react";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import AuthStore from "../../stores/AuthStore";
import { useNavigate } from "react-router-dom";
import { LoadingButton } from "@mui/lab";
import FormControl from "@mui/material/FormControl";
import FormHelperText from "@mui/material/FormHelperText";

const Login = ({ authStore }: { authStore: AuthStore }) => {
	const [credentials, setCredentials] = React.useState<{ login: string, password: string }>({ login: "", password: "" });
	const [loading, setloading] = React.useState(false);
	const [isError, setIsError] = React.useState(false);
	const [errorText, setErrorText] = React.useState("");
	const navigate = useNavigate();
	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setloading(true);
		const success = await authStore.login(credentials.login, credentials.password);
		setloading(false);
		if (success) {
			navigate("/");
		}
		else {
			setIsError(true);
			setErrorText("Неверный логин или пароль");
		}
	};

	return (
		<Box
			sx={{
				marginTop: 8,
				display: "flex",
				flexDirection: "column",
				alignItems: "center",
			}}>
			<Typography component="h1" variant="h5">
				Вход в систему
			</Typography>
			<Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }} flexDirection={"column"} display={"flex"} width={"clamp(300px, 40vw, 500px)"}>
				<FormControl>
					<TextField
						error={isError}
						margin="normal"
						required
						fullWidth
						id="login"
						label="ФИО"
						name="login"
						autoComplete="login"
						autoFocus
						value={credentials.login}
						onChange={(e) => setCredentials({ ...credentials, login: e.target.value })}
					/>
				</FormControl>
				<FormControl error>
					<TextField
						error={isError}
						margin="normal"
						required
						fullWidth
						name="password"
						label="Пароль"
						type="password"
						id="password"
						autoComplete="current-password"
						value={credentials.password}
						onChange={(e) => setCredentials({ ...credentials, password: e.target.value })}
					/>
					<FormHelperText id="component-error-text">{errorText}</FormHelperText>
				</FormControl>

				<LoadingButton loading={loading}
					type="submit"
					fullWidth
					variant="contained"
					sx={{ mt: 3, mb: 2 }}
				>
					Войти
				</LoadingButton>
			</Box>
		</Box>
	);
};

export default Login;