import { useEffect } from "react";
import StatsStore from "../../stores/StatsStore";
import { observer } from "mobx-react-lite";
import { Container, Grid, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import StatCard from "../../components/StatCard";

const Stats = observer(({ statsStore }: { statsStore: StatsStore }) => {
	useEffect(() => {
		statsStore.fetchAll();
	}, []);
	return (
		<Grid container gap={6} direction={"column"}>
			<Typography fontSize={32} fontWeight={700} color={"primary.main"}>
				Статистика
			</Typography>
			<Grid container direction={"row"} gap={3}>
				<StatCard values={[`За месяц ${statsStore.hired.month}`, `За год ${statsStore.hired.year}`]} title="Нанято сотрудников" />
				<StatCard values={[`За месяц ${statsStore.fired.month}`, `За год ${statsStore.fired.year}`]} title="Уволено сотрудников" />
				<StatCard values={[`${statsStore.totalSalary} ₽`]} title="Ожидаемые выплаты" />
			</Grid>
			<Typography fontSize={32} fontWeight={700} color={"primary.main"}>
				Ближайшие дни рождения
			</Typography>
			<Paper>
				<TableContainer>
					<Table>
						<TableHead>
							<TableRow>
								<TableCell>
								Фамилия
								</TableCell>
								<TableCell>
								Имя
								</TableCell>
								<TableCell>
								Отчество
								</TableCell>
								<TableCell>
								День рождения
								</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{
								statsStore.birthdates && statsStore.birthdates.map(birthdate =>
									(
										<TableRow key={`${birthdate.firstname}${birthdate.lastname}${birthdate.secondname}`}>
											<TableCell>
												{birthdate.firstname}
											</TableCell>
											<TableCell>
												{birthdate.lastname}
											</TableCell>
											<TableCell>
												{birthdate.secondname}
											</TableCell>
											<TableCell>
												{birthdate.birthdate.format("DD.MM.YYYY")}
											</TableCell>
										</TableRow>

									))
							}
						</TableBody>

					</Table>
				</TableContainer>
			</Paper>
		</Grid>
	);
});

export default Stats;