import * as React from "react";
import AuthStore from "../../stores/AuthStore";
import { observer } from "mobx-react-lite";
import { Paper } from "@mui/material";
import { Typography } from "@mui/material";
import { Box } from "@mui/system";



const Profile: React.FC<Props> = observer(({ authStore }) => {
	React.useEffect(() => {
		void authStore.fetchEmployee();
	}, [authStore]);

	return (
		<Paper>
			<Box width={"clamp(300px, 40vw, 500px)"} display={"flex"} flexDirection={"column"} gap={3} padding={3}>
				<Box display={"flex"} flexDirection={"column"} alignItems={"center"} gap={2}>
					<Typography variant="h4" color={"primary.main"}>Профиль</Typography>
					<Typography variant="h5">
						{`${authStore.employee?.lastname} ${authStore.employee?.firstname} ${authStore.employee?.secondname}`}
					</Typography>
				</Box>
				<Typography>
					<strong>Должность: </strong>{authStore.employee?.position}
				</Typography>
				<Typography>
					<strong>Зарплата: </strong>{authStore.employee?.salary} ₽
				</Typography>
				<Typography>
					<strong>Дата рождения: </strong>{authStore.employee?.birthdate.format("DD.MM.YYYY")}
				</Typography>
				<Typography>
					<strong>Нанят: </strong>{authStore.employee?.hiredAt.format("DD.MM.YYYY")}
				</Typography>
			</Box>
		</Paper>
	);

});
type Props = {
	authStore: AuthStore;
}
export default Profile;