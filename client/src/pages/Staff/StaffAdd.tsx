import * as React from "react";
import { useNavigate } from "react-router-dom";
import EmployeesStore from "../../stores/EmployeesStore";
import { observer } from "mobx-react-lite";
import StaffForm from "../../components/StaffForm";
import useLoadingWrapper from "../../hooks/useLoadingWrapper";


const StaffAdd = observer(({ employeesStore }: { employeesStore: EmployeesStore }) => {
	const navigate = useNavigate();
	const [loading, setLoading] = React.useState(false);
	const addEmployee = useLoadingWrapper(async () => await employeesStore.addEmployee(), setLoading);
	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const employeeWithId = await addEmployee();
		if (!employeeWithId) return;
		const { lastname, firstname, secondname } = employeesStore.currentEmployee;
		const login = `${lastname} ${firstname} ${secondname}`;
		await employeesStore.partialRegisterEmployee(employeeWithId, login);
		navigate("/staff/add/success");
	};

	return (
		<StaffForm employeesStore={employeesStore}
			handleSubmit={handleSubmit}
			loading={loading}
			title="Создать сотрудника"
			buttonText="Создать" />
	);
});

export default StaffAdd;