import * as React from "react";
import EmployeesStore from "../../stores/EmployeesStore";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableFooter from "@mui/material/TableFooter";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { observer } from "mobx-react-lite";
import AuthStore from "../../stores/AuthStore";
import { Fab, Paper, Toolbar, Typography } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import LocalFireDepartmentIcon from "@mui/icons-material/LocalFireDepartment";
import { useNavigate } from "react-router-dom";
import TablePaginationActions from "@mui/material/TablePagination/TablePaginationActions";

const Staff = observer(({ employeesStore, authStore }: { employeesStore: EmployeesStore, authStore: AuthStore }) => {
	const navigate = useNavigate();
	const [page, setPage] = React.useState(0);
	const [rowsPerPage, setRowsPerPage] = React.useState(5);
	React.useEffect(() => void employeesStore.fetchEmployees(), []);

	const canPerformActions = (employeeId: number | undefined) => authStore.user?.role === "hr" && employeeId !== authStore.user?.id;
	const handleChangePage = (_: any, newPage: number) => {
		setPage(newPage);
	};
	const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
		setRowsPerPage(parseInt(event.target.value));
		setPage(0);
	};

	return (
		<Paper sx={{ width: "100%" }}>
			<Toolbar>
				<Typography variant="h4" color={"primary.main"} mb={2}>Список сотрудников</Typography>
			</Toolbar>
			<TableContainer>
				<Table>
					<TableHead>
						<TableRow>
							<TableCell>Фамилия</TableCell>
							<TableCell>Имя</TableCell>
							<TableCell>Отчество</TableCell>
							<TableCell>Дата рождения</TableCell>
							<TableCell>Нанят</TableCell>
							<TableCell>Должность</TableCell>
							<TableCell>Зарплата</TableCell>
							{authStore.user?.role === "hr" && <TableCell align="center">Действия</TableCell>}
						</TableRow>
					</TableHead>
					<TableBody>
						{employeesStore.employees &&
							employeesStore.employees.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((employee) => (
								<TableRow key={employee.id} sx={{ "& > td, & > th": { border: 0 }, borderBottom: "1px solid rgba(224, 224, 224, 1)", height: "90px" }}>
									<TableCell>{employee.lastname}</TableCell>
									<TableCell>{employee.firstname}</TableCell>
									<TableCell>{employee.secondname}</TableCell>
									<TableCell>{employee.birthdate.format("DD.MM.YYYY")}</TableCell>
									<TableCell>{employee.hiredAt.format("DD.MM.YYYY")}</TableCell>
									<TableCell>{employee.position}</TableCell>
									<TableCell>{employee.salary}</TableCell>
									<TableCell align="center" sx={{
										display: "flex",
										gap: "10px",
										alignItems: "center",
										justifyContent: "center",
									}}>
										{canPerformActions(employee.id) &&
											<>
												<Fab color="primary" aria-label="edit" onClick={() => {
													employeesStore.setCurrentEmployee(employee);
													navigate("/staff/edit");
												}}>
													<EditIcon />
												</Fab>
												<Fab color="error" aria-label="edit" onClick={() => employee.id && employeesStore.fireAnEmployee(employee.id)}>
													<LocalFireDepartmentIcon />
												</Fab>
											</>
										}
									</TableCell>
								</TableRow>))}
					</TableBody>
					<TableFooter>
						<TableRow >
							<TablePagination
								rowsPerPageOptions={[5, 10, 25, { label: "Все", value: -1 }]}
								count={employeesStore.employees.length}
								rowsPerPage={rowsPerPage}
								page={page}
								SelectProps={{
									inputProps: {
										"aria-label": "rows per page",
									},
									native: true,
								}}
								labelRowsPerPage="Строк на страннице"
								onPageChange={handleChangePage}
								onRowsPerPageChange={handleChangeRowsPerPage}
								ActionsComponent={TablePaginationActions}
							/>
						</TableRow>
					</TableFooter>
				</Table>
			</TableContainer>
		</Paper>
	);
});

export default Staff;
