import { Box, Paper } from "@mui/material";
import Typography from "@mui/material/Typography";

const StaffAddedSuccess = () => {
	return (
		<Paper>
			<Box display={"flex"} alignItems={"center"} flexDirection={"column"} padding={3} gap={2}>
				<Typography fontSize={18}>
					Ссылка для смены пароля
				</Typography>
				<Typography fontWeight={700}>
					{localStorage.getItem("fullname")}
				</Typography>
				<Typography>
					{`${window.location.origin}/reset-password/${localStorage.getItem("token")}`}
				</Typography>
			</Box>
		</Paper>
	);
};

export default StaffAddedSuccess;