import * as React from "react";
import { useNavigate } from "react-router-dom";
import EmployeesStore from "../../stores/EmployeesStore";
import { observer } from "mobx-react-lite";
import StaffForm from "../../components/StaffForm";
import useLoadingWrapper from "../../hooks/useLoadingWrapper";

const StaffEdit = observer(({ employeesStore }: { employeesStore: EmployeesStore }) => {
	const navigate = useNavigate();
	const [loading, setLoading] = React.useState(false);
	const updateEmployee = useLoadingWrapper(async () => await employeesStore.updateEmployee(), setLoading);
	const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const employeeWithId = await updateEmployee();
		if (!employeeWithId) return;
		navigate("/staff");
	};

	return (
		<StaffForm employeesStore={employeesStore}
			handleSubmit={handleSubmit}
			loading={loading}
			title="Обновить сотрудника"
			buttonText="Обновить" />
	);
});

export default StaffEdit;