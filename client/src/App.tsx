import { CssBaseline } from "@mui/material";
import NavBar from "./components/NavBar";
import MainRouter from "./components/MainRouter";
import Main from "./components/Main";
import { Stores } from "./helpers/buildStores";

function App({stores}: {stores: Stores}) {
	return (
		<>
			<Main navBarStore={stores.navBar}>
				<MainRouter stores={stores} />
			</Main>
			<NavBar authStore={stores.auth} navBarStore={stores.navBar} />
			<CssBaseline />
		</>
	);
}

export default App;
