import { makeAutoObservable } from "mobx";
import * as React from "react";
import { $api, $auth } from "../api";
import { User } from "../types/User";
import jwt_decode from "jwt-decode";
import { Employee } from "../types/Employee";
import moment from "moment";

class AuthStore {
	authenticated = false;
	user: User | null = null;
	employee: Employee | null = null;
	
	constructor() {
		const token = localStorage.getItem("accessToken");
		if (token) {
			const payload = this.parseJwt(token);
			if (payload) {
				this.authenticated = payload.exp * 1000 > Date.now();
				this.user = { login: payload.login, id: payload.id, role: payload.role } as User;
			}
		}
		makeAutoObservable(this);
	}

	async login(login: string, password: string): Promise<boolean> {
		try {
			const response = await $auth.post("/login", {
				login,
				password,
			});
			localStorage.setItem("accessToken", response.data.accessToken);
			this.authenticated = true;
			const payload = this.parseJwt(response.data.accessToken);
			if (payload)
				this.user = { login: payload.login, id: payload.id, role: payload.role } as User;
			return true;
		} catch (e) {
			return false;
		}
	}

	async resetPassword(token: string, newPassword: string): Promise<boolean> {
		try {
			await $auth.patch(`/reset-password/${token}`, { newPassword });
			return true;
		} catch (e) {
			return false;
		}

	}

	async logout() {
		localStorage.removeItem("accessToken");
		this.authenticated = false;
		this.user = null;
	}

	async fetchEmployee() {
		try {
			if (!this.user?.id) return;
			const response = await $api.get(`/employees/${this.user.id}`);
			const employee = response.data;
			this.employee = { ...employee, birthdate: moment(employee.birthdate), hiredAt: moment(employee.hiredAt)};
		} catch (e) {
			return null;
		}

	}

	parseJwt(token: string): { login: string, id: number, role: string, exp: number, iat: number } | null {
		if (!token)
			return null;

		try {
			return jwt_decode(token);
		} catch (e) {
			return null;
		}
	}
}


export default AuthStore;

const StoreContext = React.createContext<AuthStore | null>(null);
export const StoreProvider = ({ children, store }:
	{ children: React.ReactNode | React.ReactNode[] | undefined, store: AuthStore }) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useAuthStore = () => React.useContext(StoreContext);