import { makeAutoObservable } from "mobx";
import * as React from "react";
import { $api, $auth } from "../api";
import { Employee } from "../types/Employee";
import moment from "moment";
import { Position } from "../../../shared/enums/Position";

class EmployeesStore {
	employees: Employee[] = [];
	currentEmployee: Employee;
	token: string | null = null;

	constructor() {
		const getDefaultEmployee = () => ({
			firstname: "",
			lastname: "",
			secondname: "",
			birthdate: moment(),
			hiredAt: moment(),
			position: "Сотрудник" as Position,
			salary: 0,
		} as Employee);
		this.currentEmployee = getDefaultEmployee();
		makeAutoObservable(this);
	}

	setCurrentEmployee(employee: Employee) {
		this.currentEmployee = {
			...employee,
			birthdate: moment(employee.birthdate),
			hiredAt: moment(employee.hiredAt),
			createdAt: moment(employee.createdAt),
			updatedAt: moment(employee.updatedAt),
		};
	}

	async fetchEmployees() {
		try {
			const response = await $api.get("/employees");
			this.employees = response.data;
			this.employees = this.employees.map((employee) => ({
				...employee,
				birthdate: moment(employee.birthdate),
				hiredAt: moment(employee.hiredAt),
			}));
		} catch {
			return;
		}
	}

	async addEmployee(): Promise<Employee | null> {
		try {
			const employeeResponse = await $api.post("/employees", this.currentEmployee);
			return employeeResponse.data;
		} catch (e) {
			return null;
		}
	}

	async updateEmployee() {
		try {
			const employeeResponse = await $api.put(`/employees/${this.currentEmployee.id}`, this.currentEmployee);
			return employeeResponse.data;
		} catch (e) {
			return null;
		}
	}

	async fireAnEmployee(id: number) {
		try {
			if (!id) return;
			await $api.post(`/fired/${id}`, { reason: "Уволен" });
			this.employees = this.employees.filter((employee) => employee.id !== id);
		} catch (e) {
			return;
		}
	}

	async partialRegisterEmployee(employee: Employee, login: string) {
		try {
			const tokenResponse = await $auth.post("/partial-registration", { employeeId: employee.id, login });
			if (!tokenResponse.data) return;
			const token = tokenResponse.data;
			const fullname = `${employee.lastname} ${employee.firstname} ${employee.secondname}`;
			localStorage.setItem("token", token);
			localStorage.setItem("fullname", fullname);
		} catch (e) {
			console.log(e);
		}
	}
}

export default EmployeesStore;

const StoreContext = React.createContext<EmployeesStore | null>(null);
export const StoreProvider = ({ children, store }:
	{ children: React.ReactNode | React.ReactNode[] | undefined, store: EmployeesStore }) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useEmployeesStore = () => React.useContext(StoreContext);