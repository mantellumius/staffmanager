import { makeAutoObservable } from "mobx";
import * as React from "react";
import { $api } from "../api";
import moment from "moment";

class StatsStore {
	totalSalary: number = 0;
	hired: { month: number, year: number } = { month: 0, year: 0 };
	fired: { month: number, year: number } = { month: 0, year: 0 };
	birthdates: { firstname: string, lastname: string, secondname: string, birthdate: moment.Moment }[] = [];
	constructor() {
		makeAutoObservable(this);
	}

	async fetchAll() {
		await this.fetchTotalSalary();
		await this.fetchHired();
		await this.fetchFired();
		await this.fetchBirthdates();
	}

	async fetchTotalSalary() {
		try {
			const response = await $api.get("/stats/salary");
			this.totalSalary = response.data.totalsalary;
		} catch(e) {
			return;
		}
	}

	async fetchHired() {
		try {
			const response = await $api.get("/stats/hired");
			const {hired_this_month, hired_this_year} = response.data;
			this.hired = {month: hired_this_month, year: hired_this_year};
		} catch(e) {
			return;
		}
	}

	async fetchFired() {
		try {
			const response = await $api.get("/stats/fired");
			const { fired_this_month, fired_this_year } = response.data;
			this.fired = { month: fired_this_month, year: fired_this_year };
		} catch(e) {
			return;
		}
	}

	async fetchBirthdates() {
		try {
			const response = await $api.get("/stats/birthdates");
			this.birthdates = response.data;
			this.birthdates = this.birthdates.map((birthdate) => ({...birthdate, birthdate: moment(birthdate.birthdate)}));
		} catch(e) {
			return;
		}
	}
}


export default StatsStore;

const StoreContext = React.createContext<StatsStore | null>(null);
export const StoreProvider = ({ children, store }:
	{ children: React.ReactNode | React.ReactNode[] | undefined, store: StatsStore }) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useStatsStore = () => React.useContext(StoreContext);