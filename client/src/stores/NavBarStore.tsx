import { makeAutoObservable } from "mobx";
import * as React from "react";
import Groups2Icon from "@mui/icons-material/Groups2";
import QueryStatsIcon from "@mui/icons-material/QueryStats";
import PersonAddAltIcon from "@mui/icons-material/PersonAddAlt";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

class NavBarStore {
	open: boolean;
	items: NavBarItem[];

	constructor() {
		this.open = false;
		this.items = [
			{
				id: 0,
				label: "Профиль",
				route: "/profile",
				icon: <AccountCircleIcon />,
				reqAuth: true
			},
			{
				id: 1,
				label: "Список сотрудников",
				route: "/staff",
				icon: <Groups2Icon />,
				reqAuth: true
			},
			{
				id: 2,
				label: "Добавить сотрудника",
				route: "/staff/add",
				icon: <PersonAddAltIcon />,
				reqAuth: true,
				reqRole: "hr",
			},
			{
				id: 3,
				label: "Статистика",
				route: "/stats",
				icon: <QueryStatsIcon />,
				reqAuth: true,
				reqRole: "hr",
			}
		];
		makeAutoObservable(this);
	}

	toggleAppBar() {
		this.open = !this.open;
	}
}
type NavBarItem = {
	id: number;
	label: string;
	route: string;
	icon: JSX.Element;
	reqAuth: boolean;
	reqRole?: string;
}

export default NavBarStore;

const StoreContext = React.createContext<NavBarStore | null>(null);
export const StoreProvider = ({ children, store }:
	{ children: React.ReactNode | React.ReactNode[] | undefined, store: NavBarStore }) => {
	return (
		<StoreContext.Provider value={store} >
			{children}
		</StoreContext.Provider>
	);
};
export const useNavBarStore = () => React.useContext(StoreContext);