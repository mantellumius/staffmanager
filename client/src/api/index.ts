import axios from "axios";

//@ts-ignore
export const URL = import.meta.env.VITE_API_URL as string ?? "http://localhost:5000";
export const API_URL = `${URL}/api`;
export const AUTH_URL = `${URL}/auth`;

export const $auth = axios.create({
	withCredentials: true,
	baseURL: AUTH_URL
});
export const $api = axios.create({
	withCredentials: true,
	baseURL: API_URL
});
$api.interceptors.request.use((config) => {
	config.headers.setAuthorization(`Bearer ${localStorage.getItem("accessToken")}`);
	return config;
});
$auth.interceptors.request.use((config) => {
	config.headers.setAuthorization(`Bearer ${localStorage.getItem("accessToken")}`);
	return config;
});
